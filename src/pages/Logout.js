import {useContext, useEffect} from 'react';
import {Navigate} from 'react-router-dom';
import UserContext from '../UserContext';

export default function Logout() {

	// Allows us to consume the UserContext objext and destructure it to access the user state and unsetUser function from the context provider.
	const {unsetUser, setUser} = useContext(UserContext);

	// This will clear the localStorage of the user's information
	unsetUser();

	useEffect(() => {
		// Set the user state back to its original value
		setUser({id: null})
	}, [])


	// localStorage.clear() - no need this since we have unsetUser in app.js

	return (

			<Navigate to="/" />

		)

}