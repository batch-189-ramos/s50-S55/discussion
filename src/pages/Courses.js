import {Fragment, useEffect, useState} from 'react';
import CourseCard from '../components/CourseCard';
// import courseData from '../data/coursesData'; - mock database only


export default function Courses() {

	/*console.log(courseData)
	console.log(courseData[0])*/

	const [courses, setCourses] = useState([]);

	useEffect(()=> {
		fetch('http://localhost:4000/courses/')
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setCourses(data.map(course => {
				return (
						<CourseCard key={course.id} courseProp={course}/>
					)
			}))
		})
	}, [])

	/*const courses = courseData.map(course => {
		return (// key special string
				<CourseCard key={course.id} courseProp={course}/>
			)
	})
*/
	return(

		
		<Fragment>
			<h1 className="mb-3 mt-3">Courses</h1>
			{courses}
		</Fragment>

		)


}